﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamoDbViewer.Enums
{
    public enum ExplorableTables
    {
        run,
        masterData,
        user,
        signature,
        item,
        PBWorkingSpace
    }
}
