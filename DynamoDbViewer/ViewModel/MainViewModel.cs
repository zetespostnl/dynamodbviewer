using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Threading;
using System.Drawing;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Zetes.DomainModel.DataAccessContracts;
using Zetes.DomainModel.DataAccessImplementation.Configuration;
using Zetes.DomainModel.DataAccessImplementation.Repositories;

using DynamoDbViewer.Enums;
using DynamoDbViewer.Helpers;
using Zetes.DomainModel.Entities;
using Zetes.DomainModel.Entities.MasterData;
using Zetes.DomainModel.Entities.UserManagement;
using Newtonsoft.Json;
using System.Timers;
using System;
using System.Threading.Tasks;
using Zetes.DomainModel.DataAccessImplementation.Entities;
using DynamoDbViewer.ValueConverters;
using System.IO;
using System.Reflection;
using Zetes.DomainModel.DataAccessImplementation.TableDefinitions;
using System.Configuration;
using ZetesChronos.Libraries.SystemLog;
using Zetes.DomainModel.DataAccessImplementation;
using ZetesChronos.Integration.Host.LocalDrive;
using Zetes.DomainModel.Entities.Execution;

namespace DynamoDbViewer.ViewModel
{

    public class MainViewModel : ViewModelBase
    {
        #region Cosnts    
        #endregion
        #region Fields
        private Visibility _ImageConverterVisibility = Visibility.Collapsed;
        private string _Byte64ImageString;
        private bool _isBusy = false;
        private string _endPoint = "http://localhost:8000";
        private string _accessKey = "AKIAIQ5562Q6DC45AU6A";
        private string _secretKey = "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";
        private string _jsonText = "";
        private ObservableCollection<string> _myProperty = new ObservableCollection<string>() { "run", "masterData", "user", "signature", "PBWorkingSpace" };
        private string _selectedTable;

        private IRestServiceLogger _logger;
        private IRunRepository _runRepository;
        private IMasterDataRepository _masterDataRepository;
        private IUserRepository _userRepository;
        private ISignatureRepository _signatureRepository;
        
        private string _externalId = "";
        private string _somethingWrongMessege = "Exception message";
        private Visibility _warningMessageVisibility = Visibility.Collapsed;
        private Visibility _UserIdVisibility = Visibility.Collapsed;
        private string _userId = "";
        private string _tablesPrefix = "";
        #endregion
        #region Properties  
        public string TablesPrefix
        {
            get
            {
                return _tablesPrefix;
            }

            set
            {
                if (_tablesPrefix == value)
                {
                    return;
                }

                _tablesPrefix = value;
                RaisePropertyChanged();
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public Visibility ImageConverterVisibility
        {
            get
            {
                return _ImageConverterVisibility;
            }

            set
            {
                if (_ImageConverterVisibility == value)
                {
                    return;
                }

                _ImageConverterVisibility = value;
                RaisePropertyChanged();
            }
        }
        public string Byte64ImageString
        {
            get
            {
                return _Byte64ImageString;
            }

            set
            {
                if (_Byte64ImageString == value)
                {
                    return;
                }

                _Byte64ImageString = value;
                RaisePropertyChanged();
            }
        }
        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }

            set
            {
                if (_isBusy == value)
                {
                    return;
                }

                _isBusy = value;
                RaisePropertyChanged();
            }
        }
        public Visibility UserIdVisibility
        {
            get
            {
                return _UserIdVisibility;
            }

            set
            {
                if (_UserIdVisibility == value)
                {
                    return;
                }

                _UserIdVisibility = value;
                RaisePropertyChanged();
            }
        }

        public string UserId
        {
            get
            {
                return _userId;
            }

            set
            {
                if (_userId == value)
                {
                    return;
                }

                _userId = value;
                RaisePropertyChanged();
            }
        }
        public Visibility WarningMessageVisibility
        {
            get { return _warningMessageVisibility; }

            set
            {
                if (_warningMessageVisibility == value)
                {
                    return;
                }

                _warningMessageVisibility = value;
                RaisePropertyChanged();
                if (_warningMessageVisibility == Visibility.Visible)
                {
                    var timer = new System.Timers.Timer();
                    timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                    timer.Interval = 9000;
                    timer.Enabled = true;
                }
            }
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            WarningMessageVisibility = Visibility.Collapsed;
        }

        public string SomethingWrongMessege
        {
            get
            {
                return _somethingWrongMessege;
            }

            set
            {
                if (_somethingWrongMessege == value)
                {
                    return;
                }

                _somethingWrongMessege = value;
                RaisePropertyChanged();
            }
        }
        public string SelectedTable
        {
            get
            {
                return _selectedTable;
            }

            set
            {
                if (_selectedTable == value)
                {
                    return;
                }

                _selectedTable = value;
                RaisePropertyChanged();
                UserIdVisibility = SelectedTable == "user" ? Visibility.Visible : Visibility.Collapsed;
                ImageConverterVisibility = SelectedTable == "signature" ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        public ObservableCollection<string> TablesCollection
        {
            get
            {
                return _myProperty;
            }

            set
            {
                if (_myProperty == value)
                {
                    return;
                }

                _myProperty = value;
                RaisePropertyChanged();
            }
        }
        public string JsonText
        {
            get
            {
                return _jsonText;
            }

            set
            {
                if (_jsonText == value)
                {
                    return;
                }

                _jsonText = value;
                RaisePropertyChanged();
            }
        }
        public string EndPoint
        {
            get
            {
                return _endPoint;
            }

            set
            {
                if (_endPoint == value)
                {
                    return;
                }

                _endPoint = value;
                RaisePropertyChanged();
            }
        }
        public string AccessKey
        {
            get
            {
                return _accessKey;
            }

            set
            {
                if (_accessKey == value)
                {
                    return;
                }

                _accessKey = value;
                RaisePropertyChanged();
            }
        }
        public string SecrertKey
        {
            get
            {
                return _secretKey;
            }

            set
            {
                if (_secretKey == value)
                {
                    return;
                }

                _secretKey = value;
                RaisePropertyChanged();
            }
        }
        public string ExternalId
        {
            get
            {
                return _externalId;
            }

            set
            {
                if (_externalId == value)
                {
                    return;
                }

                _externalId = value;
                RaisePropertyChanged();
            }
        }
        #endregion
        #region Commands
        private RelayCommand _updateSettingsCommand;
        public RelayCommand UpdateSettingsCommand
        {
            get
            {
                return _updateSettingsCommand
                    ?? (_updateSettingsCommand = new RelayCommand(
                    () =>
                    {
                        InitializeConfig();
                    }));
            }
        }        
        private RelayCommand _deleteObjectCommand;
        public RelayCommand DeleteObjectCommand
        {
            get
            {
                return _deleteObjectCommand
                    ?? (_deleteObjectCommand = new RelayCommand(
                    async () =>
                    {
                        try
                        {
                            bool deleted = false;
                            IsBusy = true;
                            await Task.Run(() =>
                            {
                                switch (SelectedTable)
                                {
                                    case nameof(ExplorableTables.run):

                                        var run = _runRepository.Get(ExternalId);
                                        deleted = _runRepository.Delete(run);
                                        ShowWarningMessage(deleted ? "deleted" : "something went wrong!");

                                        break;
                                    case nameof(ExplorableTables.masterData):

                                        var md = _masterDataRepository.Get(ExternalId);
                                        deleted = _masterDataRepository.Delete(md);
                                        ShowWarningMessage(deleted ? "deleted" : "something went wrong!");

                                        break;
                                    case nameof(ExplorableTables.signature):

                                        var sgn = _signatureRepository.Get(ExternalId);
                                        deleted = _signatureRepository.Delete(sgn);
                                        ShowWarningMessage(deleted ? "deleted" : "something went wrong!");
                                        break;
                                    //case nameof(ExplorableTables.PBWorkingSpace):

                                    //    var pbW = _pbWorkingSpaceRepository.Get(ExternalId);
                                    //    deleted = _pbWorkingSpaceRepository.Delete(pbW);
                                    //    ShowWarningMessage(deleted ? "deleted" : "something went wrong!");
                                    //    break;
                                    case nameof(ExplorableTables.user):

                                        ShowWarningMessage("Not implemented yet!");
                                        break;
                                }
                            });
                            IsBusy = false;
                        }
                        catch (Exception ex)
                        {
                            ShowWarningMessage(ex.Message);
                            IsBusy = false;
                        }
                    }));
            }
        }
        private RelayCommand _addObjectCommand;
        public RelayCommand AddObjectCommand
        {
            get
            {
                return _addObjectCommand
                    ?? (_addObjectCommand = new RelayCommand(
                    () =>
                    {
                        // TODO incase
                    }));
            }
        }
        private RelayCommand _convertImageCommand;
        public RelayCommand ConvertImageCommand
        {
            get
            {
                return _convertImageCommand
                    ?? (_convertImageCommand = new RelayCommand(
                    () =>
                    {
                        Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                        dlg.DefaultExt = ".png";
                        dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
                        var result = dlg.ShowDialog();


                        if (result == true)
                        {
                            IsBusy = true;
                            Task.Run(() =>
                            {
                                using (Image image = Image.FromFile(dlg.FileName))
                                {
                                    using (MemoryStream m = new MemoryStream())
                                    {
                                        image.Save(m, image.RawFormat);
                                        byte[] imageBytes = m.ToArray();

                                        // Convert byte[] to Base64 String
                                        string base64String = Convert.ToBase64String(imageBytes);
                                        Byte64ImageString = base64String;
                                    }
                                }
                            });
                            IsBusy = false;
                        }
                    }));
            }
        }
        private RelayCommand _copyToClipboardCommand;
        public RelayCommand CopyToClipboardCommand
        {
            get
            {
                return _copyToClipboardCommand
                    ?? (_copyToClipboardCommand = new RelayCommand(
                    () =>
                    {
                        System.Windows.Clipboard.SetText(Byte64ImageString);
                        Byte64ImageString = string.Empty;
                    }));
            }
        }
        private RelayCommand _mainWindowLoadedCommand;
        public RelayCommand MainWindowLoadedCommand
        {
            get
            {
                return _mainWindowLoadedCommand
                    ?? (_mainWindowLoadedCommand = new RelayCommand(
                    () =>
                    {
                    }));
            }
        }
        private RelayCommand _saveObjectCommand;
        public RelayCommand SaveObjectCommand
        {
            get
            {
                return _saveObjectCommand
                    ?? (_saveObjectCommand = new RelayCommand(
                    async () =>
                    {
                        try
                        {
                            IsBusy = true;
                            await Task.Run(() =>
                            {
                                switch (SelectedTable)
                                {
                                    case nameof(ExplorableTables.run):
                                        if (JsonText.ValidateJSON())
                                        {
                                            var run = RunJsonConverter.ToRun(JsonConvert.DeserializeObject(JsonText));
                                            _runRepository.Save(run);
                                            run = _runRepository.Get(ExternalId);
                                            if (run != null)
                                                JsonText = Newtonsoft.Json.JsonConvert.SerializeObject(run, Formatting.Indented);
                                            else
                                                ShowWarningMessage("Empty run");
                                        }
                                        else
                                        {
                                            ShowWarningMessage("Invalide or empry json");
                                        }
                                        break;
                                    case nameof(ExplorableTables.masterData):
                                        if (JsonText.ValidateJSON())
                                        {
                                            var md = Newtonsoft.Json.JsonConvert.DeserializeObject<MasterData>(JsonText);
                                            _masterDataRepository.Save(md);
                                            md = _masterDataRepository.Get(ExternalId);
                                            if (md != null)
                                                JsonText = Newtonsoft.Json.JsonConvert.SerializeObject(md, Formatting.Indented);
                                            else
                                                ShowWarningMessage("Empty master data");
                                        }
                                        else
                                        {
                                            ShowWarningMessage("Invalide or empry json");
                                        }
                                        break;
                                    case nameof(ExplorableTables.signature):
                                        if (JsonText.ValidateJSON())
                                        {
                                            var sgn = Newtonsoft.Json.JsonConvert.DeserializeObject<Signature>(JsonText);
                                            _signatureRepository.Save(sgn);
                                            sgn = _signatureRepository.Get(ExternalId);
                                            if (sgn != null)
                                                JsonText = Newtonsoft.Json.JsonConvert.SerializeObject(sgn, Formatting.Indented);
                                            else
                                                ShowWarningMessage("Empty signature");
                                        }
                                        else
                                        {
                                            ShowWarningMessage("Invalide or empry json");
                                        }
                                        break;
                                    //case nameof(ExplorableTables.PBWorkingSpace):
                                    //    if (JsonText.ValidateJSON())
                                    //    {
                                    //        var pbw = Newtonsoft.Json.JsonConvert.DeserializeObject<PBWorkingSpace>(JsonText);
                                    //        _pbWorkingSpaceRepository.Save(pbw);
                                    //        pbw = _pbWorkingSpaceRepository.Get(ExternalId);
                                    //        if (pbw != null)
                                    //            JsonText = Newtonsoft.Json.JsonConvert.SerializeObject(pbw, Formatting.Indented);
                                    //        else
                                    //            ShowWarningMessage("Empty PBW");
                                    //    }
                                    //    else
                                    //    {
                                    //        ShowWarningMessage("Invalide or empry json");
                                    //    }
                                    //    break;
                                    //case nameof(ExplorableTables.user):
                                    //    if (JsonText.ValidateJSON())
                                    //    {
                                    //        var user = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(JsonText);
                                    //        _userRepository.SaveUser(user);
                                    //        user = _userRepository.GetUserById(ExternalId, UserId);
                                    //        if (user != null)
                                    //            JsonText = Newtonsoft.Json.JsonConvert.SerializeObject(user, Formatting.Indented);
                                    //        else
                                    //            ShowWarningMessage("Empty user");
                                    //    }
                                    //    else
                                    //    {
                                    //        ShowWarningMessage("Invalide or empry json");
                                    //    }
                                    //    break;
                                }
                            });
                            IsBusy = false;
                        }
                        catch (Exception ex)
                        {
                            ShowWarningMessage(ex.Message);
                            IsBusy = false;
                        }
                    }));
            }
        }
        private RelayCommand _getObjectCommand;
        public RelayCommand GetObjectCommand
        {
            get
            {
                return _getObjectCommand
                    ?? (_getObjectCommand = new RelayCommand(
                    async () =>
                    {
                        try
                        {
                            IsBusy = true;
                            await Task.Run(() =>
                            {
                                switch (SelectedTable)
                                {
                                    case nameof(ExplorableTables.run):
                                        var run = _runRepository.Get(ExternalId);
                                        if (run != null)

                                            JsonText = Newtonsoft.Json.JsonConvert.SerializeObject(run, Formatting.Indented);
                                        else
                                        {
                                            ShowWarningMessage("Empty run");
                                            JsonText = "";
                                        }

                                        break;
                                    case nameof(ExplorableTables.masterData):
                                        var md = _masterDataRepository.Get(ExternalId);
                                        if (md != null)
                                            JsonText = Newtonsoft.Json.JsonConvert.SerializeObject(md, Formatting.Indented);
                                        else
                                        {
                                            ShowWarningMessage("Empty md");
                                            JsonText = "";
                                        }
                                        break;
                                    case nameof(ExplorableTables.signature):
                                        var sgn = _signatureRepository.Get(ExternalId);
                                        if (sgn != null)
                                            JsonText = Newtonsoft.Json.JsonConvert.SerializeObject(sgn, Formatting.Indented);
                                        else
                                        {
                                            ShowWarningMessage("Empty signature");
                                            JsonText = "";
                                        }
                                        break;
                                    //case nameof(ExplorableTables.PBWorkingSpace):
                                    //    var pbw = _pbWorkingSpaceRepository.Get(ExternalId);
                                    //    if (pbw != null)
                                    //        JsonText = Newtonsoft.Json.JsonConvert.SerializeObject(pbw, Formatting.Indented);
                                    //    else
                                    //    {
                                    //        ShowWarningMessage("Empty PBW");
                                    //        JsonText = "";
                                    //    }
                                    //    break;
                                    case nameof(ExplorableTables.user):
                                        //var usr = _userRepository.GetUserById(ExternalId, UserId);
                                        //if (usr != null)
                                        //    JsonText = Newtonsoft.Json.JsonConvert.SerializeObject(usr, Formatting.Indented);
                                        //else
                                        //{
                                        //    ShowWarningMessage("Empty user");
                                        //    JsonText = "";
                                        //}
                                        break;
                                }
                            });
                            IsBusy = false;
                        }
                        catch (Exception ex)
                        {
                            ShowWarningMessage(ex.Message);
                            IsBusy = false;
                        }
                    }));
            }
        }

        private RelayCommand _connectToDbCommand;
        private IRepositoryFactory repositoryFactory;

        public RelayCommand ConnectToDbCommand
        {
            get
            {
                return _connectToDbCommand
                    ?? (_connectToDbCommand = new RelayCommand(
                    () =>
                    {
                        try
                        {
                            SetUpConnection();
                        }
                        catch (Exception ex)
                        {
                            ShowWarningMessage(ex.Message);
                        }
                    }));
            }
        }
        #endregion
        #region Ctors        
        public MainViewModel()
        {
            SetUpConnection();
            InitializeConfig();
        }
        #endregion
        #region Helpers

        private void ShowWarningMessage(string message)
        {
            SomethingWrongMessege = message;
            WarningMessageVisibility = Visibility.Visible;
        }

        private void SetUpConnection()
        {
            DynamoDALConfiguration.Initialize(EndPoint, AccessKey, SecrertKey);
            repositoryFactory = new DynamoRepositoryFactory(new LocalHostIntegrationFactory("~/LocalStorage" + "\\executionevents.txt"));

            _logger = new NLogLogger("DbViewerLogger");
            _runRepository = repositoryFactory.CreateRunRepository(_logger);
            _masterDataRepository = repositoryFactory.CreateMasterDataRepository(_logger);
            _userRepository = repositoryFactory.CreateUserRepository(_logger);
            _signatureRepository = repositoryFactory.CreateSignatureRepository(_logger);           
        }

        private void InitializeConfig()
        {
            Version schemaVersion = Version.Parse(ConfigurationManager.AppSettings["Dynamo:SchemaVersion"]);
            DynamoTableConfiguration dynamoTableConfiguration =
                     new DynamoTableConfiguration
                     {
                         RunTableReadCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:RunTable.RCU"]),
                         RunTableWriteCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:RunTable.WCU"]),
                         ItemReferencesTableReadCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:ItemReferencesTable.RCU"]),
                         ItemReferencesTableWriteCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:ItemReferencesTable.WCU"]),
                         MasterDataTableReadCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:MasterDataTable.RCU"]),
                         MasterDataTableWriteCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:MasterDataTable.WCU"]),
                         UserDataTableReadCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:UserDataTable.RCU"]),
                         UserDataTableWriteCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:UserDataTable.WCU"]),
                         SignatureTableReadCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:SignatureTable.RCU"]),
                         SignatureTableWriteCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:SignatureTable.WCU"]),
                         PBWorkingSpaceTableReadCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:PBWorkingSpaceTable.RCU"]),
                         PBWorkingSpaceTableWriteCapacity = long.Parse(ConfigurationManager.AppSettings["Dynamo:PBWorkingSpaceTable.WCU"]),
                         TableNamePrefix = ConfigurationManager.AppSettings["AWS.DynamoDBContext.TableNamePrefix"]
                     };
            DynamoDALConfiguration.UpdateDatabaseSchema(schemaVersion, true, dynamoTableConfiguration);
        }
        #endregion
    }
}