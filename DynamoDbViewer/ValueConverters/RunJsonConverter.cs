﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Zetes.DomainModel.Entities;
using Zetes.DomainModel.Entities.Execution;
using Zetes.DomainModel.Entities.Execution.Enums;

namespace DynamoDbViewer.ValueConverters
{
    public static class RunJsonConverter
    {
        public static Run ToRun(dynamic anonyRun)
        {
            Run result = new Run
            {
                Id = anonyRun.ExternalId,
                RunNumber = anonyRun.RunNumber,
                //ExternalSystem = (ExternalSystem)anonyRun.ExternalSystem,
                Name = anonyRun.Name,
                Description = anonyRun.Description,
                Date = (DateTime)anonyRun.Date,
                Imported = (DateTime)anonyRun.Imported,
                Accepted = (DateTime?)anonyRun.Accepted,
                Started = (DateTime?)anonyRun.Started,
                Finished = (DateTime?)anonyRun.Finished,
                Released = (DateTime?)anonyRun.Released,
                //Remark = anonyRun.Remark,
                //Status = (RunStatus)anonyRun.Status,
                //ExportStatus = (ExportStatus)anonyRun.ExportStatus,
                IsBlocked = (bool)anonyRun.IsBlocked,
                //Mandatory = (bool)anonyRun.Mandatory,
                DeviceSerialNumber = anonyRun.DeviceSerialNumber,
                UserDisplayName = anonyRun.UserDisplayName,
                Shift = anonyRun.Shift,
                //HasIncident = (bool)anonyRun.HasIncident,
                LastActionTime = (DateTime)anonyRun.LastActionTime,
                UserId = anonyRun.UserId.ToString(),
                CompanyId = anonyRun.CompanyId.ToString(),
                //DepotId = anonyRun.DepotId,
                DistributionChannelId = anonyRun.DistributionChannelId,
                PlanDeskId = anonyRun.PlanDeskId,
                OwnerId = anonyRun.OwnerId,
                OwnerCompanyDisplayInfo = anonyRun.OwnerCompanyDisplayInfo,
                //TTL = (long)anonyRun.TTL,
                //Version = (long?)anonyRun.Version,
                //RunLocation = (OperationStage)anonyRun.RunLocation,
                IsTVI = (bool)anonyRun.IsTVI,
                //DayOfWeek = (int)anonyRun.DayOfWeek,
                TVIStartTime = (DateTime?)anonyRun.TVIStartTime,
                TVIEndTime = (DateTime?)anonyRun.TVIEndTime,
                IsTviConfirmed = (bool)anonyRun.IsTviConfirmed
            };


            var runAttList = new List<EntityAttribute>();
            if (anonyRun.RunAttributes != null)
            {
                foreach (var runAttribute in (IEnumerable<dynamic>)anonyRun.RunAttributes)
                {
                    runAttList.Add(ToAttribute(runAttribute));
                }
            }
            //result.RunAttributes = runAttList;


            if (anonyRun.EventMessages != null)
            {
                List<EventMessage> evtsTmp = new List<EventMessage>();
                foreach (var evtMsg in (IEnumerable<dynamic>)anonyRun.EventMessages)
                {
                    var e = ToEvent(evtMsg);
                    //result.AddEvent(e);
                    evtsTmp.Add(e);
                }
                //result.SetEventMessages(evtsTmp);
            }
            if (anonyRun.Stops != null)
            {
                List<Stop> stopsTmp = new List<Stop>();
                foreach (var stop in (IEnumerable<dynamic>)anonyRun.Stops)
                {
                    Stop s = ToStop(stop);
                    //s.SetParentRun(result);
                    //result.AddStop(s);
                    stopsTmp.Add(s);
                }
                //result.SetStops(stopsTmp);
            }

            if (anonyRun.Items != null)
            {
                List<Item> itemsTmp = new List<Item>();
                foreach (var itm in (IEnumerable<dynamic>)anonyRun.Items)
                {
                    //var i = ToItem(itm, result.Stops); /**/
                    //result.AddItem(i);
                    //itemsTmp.Add(i);
                }
                //result.SetItems(itemsTmp);
            }



            return result;
        }
        public static Stop ToStop(dynamic anonyStp)
        {
            var stop = new Stop();

            var stpsAttList = new List<EntityAttribute>();
            if (anonyStp.StopAttributes != null)
            {
                foreach (var stpAtt in (IEnumerable<dynamic>)anonyStp.StopAttributes)
                {
                    stpsAttList.Add(ToAttribute(stpAtt));
                }
            }
            //stop.StopAttributes = stpsAttList;

            var stopsIn1List = new List<Stop>();
            if (anonyStp.Stops != null)
            {
                foreach (var stpIn in (IEnumerable<dynamic>)anonyStp.Stops)
                {
                    stopsIn1List.Add(ToStop(stop));
                }
            }
            //stop.Stops = stopsIn1List;

            stop.Started = (DateTime?)anonyStp.Started;
            stop.Finished = (DateTime?)anonyStp.Finished;

            stop.Id = anonyStp.Id;
            //stop.ExportStatus = (ExportStatus)anonyStp.ExportStatus;
            stop.Name = anonyStp.Name;
            //stop.Status = (StopStatus)anonyStp.Status;
            stop.CustomerName = anonyStp.CustomerName;
            stop.Street = anonyStp.Street;
            stop.HouseNumber = (int)anonyStp.HouseNumber;
            stop.HouseNumberAdd = anonyStp.HouseNumberAdd;
            stop.Zipcode = anonyStp.Zipcode;
            stop.City = anonyStp.City;
            stop.Address = anonyStp.Address;
            stop.Address2 = anonyStp.Address2;
            stop.CountryCode = anonyStp.CountryCode;
            stop.Description = anonyStp.Description;
            stop.Sequence = (int)anonyStp.Sequence;
            stop.PTA = (DateTime)anonyStp.PTA;
            //stop.POBoxTypeOfReport = (int)anonyStp.POBoxTypeOfReport;
            //stop.GpsLatitude = (decimal)anonyStp.GpsLatitude;
            //stop.GpsLongitude = (decimal)anonyStp.GpsLongitude;
            //stop.PrevExecuted = (bool)anonyStp.PrevExecuted;
            stop.Before = (DateTime?)anonyStp.Before;
            stop.After = (DateTime?)anonyStp.After;
            stop.TVIStopType = (StopType)anonyStp.TVIStopType;
            stop.TVISequence = anonyStp.TVISequence;
            stop.TVIStart = anonyStp.TVIStart;
            stop.TVIEnd = anonyStp.TVIEnd;
            stop.TVIDuration = (int)anonyStp.TVIDuration;
            if (anonyStp.OriginalRunInfo != null)
                stop.OriginalRunInfo = ToOri(anonyStp.OriginalRunInfo);

            if (anonyStp.EventMessages != null)
            {
                foreach (var evtMsg in (IEnumerable<dynamic>)anonyStp.EventMessages)
                {
                    //stop.AddEvent(ToEvent(evtMsg));
                }
            }

            stop.ETA = (DateTime)anonyStp.ETA;
            stop.ReOpened = (bool)anonyStp.ReOpened;

            if (anonyStp.BusinessProcesses != null)
            {
                foreach (var bp in (IEnumerable<dynamic>)anonyStp.BusinessProcesses)
                {
                    BusinessProcess b = ToPb(bp);
                //    b.SetParentStop(stop);
                //    stop.AddBusinessProcess(b);
                }

            }

            return stop;
        }

        private static OriginalRunInfo ToOri(dynamic originalRunInfo)
        {
            return new OriginalRunInfo()
            {
                RunId = originalRunInfo.Id,
                DayOfWeek = (int)originalRunInfo.DayOfWeek,
                DepotId = originalRunInfo.DepotId,
                DistributionChannelId = originalRunInfo.DistributionChannelId,
                RunName = originalRunInfo.RunName,
                RunNumber = originalRunInfo.RunNumber
            };

        }

        public static BusinessProcess ToPb(dynamic anonyBp)
        {
            var bp = new BusinessProcess();

            bp.Id = anonyBp.Id;

            bp.Sequence = (int)anonyBp.Sequence;
            bp.PrevExecuted = (bool)anonyBp.PrevExecuted;
            bp.Comment = anonyBp.Comment;
            //bp.BPType = (BusinessProcessType)anonyBp.BPType;
            //bp.Status = (BusinessProcessStatus)anonyBp.Status;
            //bp.CanReopen = (bool)anonyBp.CanReopen;
            //bp.GpsLatitude = (decimal)anonyBp.GpsLatitude;
            //bp.GpsLongitude = (decimal)anonyBp.GpsLongitude;

            //bp.OrderNumber = anonyBp.OrderNumber;
            //bp.OrderName = anonyBp.OrderName;
            //bp.OrderCustomerName = anonyBp.OrderCustomerName;
            //bp.OrderRemarks = anonyBp.OrderRemarks;
            bp.Target = (DateTime)anonyBp.Target;
            bp.Before = (DateTime)anonyBp.Before;
            bp.After = (DateTime)anonyBp.After;



            var bpAttsList = new List<EntityAttribute>();
            if (anonyBp.bpAtts != null)
            {
                foreach (var bpAtt in (IEnumerable<dynamic>)anonyBp.bpAtts)
                {
                    bpAttsList.Add(ToAttribute(bpAtt));
                }
            }
            //bp.BPAttributes = bpAttsList;

            if (anonyBp.EventMessages != null)
            {
                foreach (var evtMsg in (IEnumerable<dynamic>)anonyBp.EventMessages)
                {
                    //bp.AddEvent(ToEvent(evtMsg));
                }
            }

            return bp;
        }
        public static Item ToItem(dynamic anonyItem, IEnumerable<Stop> stops)
        {
            var result = new Item();
            if (anonyItem.EventMessages != null)
            {
                foreach (var evtMsg in (IEnumerable<dynamic>)anonyItem.EventMessages)
                {
                    //result.AddEvent(ToEvent(evtMsg));
                }

            }
            result.Barcode = anonyItem.Barcode;
            result.Id = anonyItem.Id;

            //result.RunId = anonyItem.RunId;
            //result.PlannedRouteId = anonyItem.PlannedRouteId;
            //result.StopId = anonyItem.StopId;
            //result.LastDeviceSN = anonyItem.LastDeviceSN;
            //result.LastUserID = anonyItem.LastUserID;
            result.LastCOD = (decimal?)anonyItem.LastCOD;

            //result.BusinessProcessId = anonyItem.BusinessProcessId.ToString();

            //result.ArticleId = anonyItem.ArticleId;
            result.ItemDescription = anonyItem.ItemDescription;
            //result.ProductDescription = anonyItem.ProductDescription;
            //result.Status = (ItemStatus)anonyItem.Status;
            //result.ExportStatus = (ExportStatus)anonyItem.ExportStatus;
            //result.Quantity = (int)anonyItem.Quantity;
            //result.ItemType = (ItemType)anonyItem.ItemType;
            result.Volume = (decimal)anonyItem.Volume;
            result.Weight = (decimal)anonyItem.Weight;

            var itmAttList = new List<EntityAttribute>();
            if (anonyItem.ItemAttributes != null)
            {
                foreach (var itmAttribute in (IEnumerable<dynamic>)anonyItem.ItemAttributes)
                {
                    itmAttList.Add(ToAttribute(itmAttribute));
                }
            }
            //result.ItemAttributes = itmAttList;

            var apsAttList = new List<Aps>();
            if (anonyItem.APSs != null)
            {
                foreach (var apsAtt in (IEnumerable<dynamic>)anonyItem.APSs)
                {
                    apsAttList.Add(ToAps(apsAtt));
                }
            }
            //result.APSs = apsAttList;

            //result.Items = anonyItem.Items; //TODO
            //result.IsDropOffPoint = (bool)anonyItem.IsDropOffPoint;
            //result.MultiColloNumber = (int)anonyItem.MultiColloNumber;
            //result.MultiColloMaster = anonyItem.MultiColloMaster;
            //result.MasterBundle = anonyItem.MasterBundle;
            //result.IsNMG = (bool)anonyItem.IsNMG;
            //result.CODAmount = (decimal)anonyItem.CODAmount;
            //result.CODRegistered = (decimal)anonyItem.CODRegistered;
            //result.CODMatch = (bool)anonyItem.CODMatch;
            //result.APSDataComplete = (bool)anonyItem.APSDataComplete;
            //result.RecipientName = anonyItem.RecipientName;
            //result.Address = anonyItem.Address;
            //result.ZipCode = anonyItem.ZipCode;
            //result.City = anonyItem.City;
            //result.LastActionTime = (DateTime)anonyItem.LastActionTime;
            //result.CODAllowed = anonyItem.CODAllowed;

            //foreach (var ss in stops)
            //{
            //    var bb = ss.BusinessProcesses.FirstOrDefault(b => b.ExternalId == anonyItem.BusinessProcessId.ToString());
            //    bb?.AddItem(result);
            //}

            return result;
        }
        public static Aps ToAps(dynamic anonyAps)
        {
            var aps = new Aps();

            aps.ApsType = (int)anonyAps.ApsType;

            aps.Id = anonyAps.Id;
            aps.Sequence = (int)anonyAps.Sequence;
            aps.Level = anonyAps.Level;
            var apsAttList = new List<EntityAttribute>();
            if (anonyAps.ApsAttributes != null)
            {
                foreach (var apsAttribute in (IEnumerable<dynamic>)anonyAps.ApsAttributes)
                {
                    apsAttList.Add(ToAttribute(apsAttribute));
                }
            }
            //aps.ApsAttributes = apsAttList;

            return aps;
        }

        public static EntityAttribute ToAttribute(dynamic anonyAttr)
        {
            return new EntityAttribute()
            {                
                KeyField = anonyAttr.KeyField.ToString(),
                ValueField = anonyAttr.ValueField.ToString()
            };
        }
        public static EventMessage ToEvent(dynamic anonyEvnt)
        {
            return new EventMessage()
            {
                UserId = anonyEvnt.UserId,
                //Id = anonyEvnt.Id,
                TimeStamp = (DateTime?)anonyEvnt.TimeStamp,
                GpsLatitude = (decimal)anonyEvnt.GpsLatitude,
                GpsLongitude = (decimal)anonyEvnt.GpsLongitude,
                Type = anonyEvnt.Type,
                StopId = anonyEvnt.StopId,
                BusinessProcessId = anonyEvnt.BusinessProcessId,
                ItemId = anonyEvnt.ItemId,
                NewStatus = (int)anonyEvnt.NewStatus,
                Comment = anonyEvnt.Comment,
                PhotoFileName = anonyEvnt.PhotoFileName,
                ExtraParams = anonyEvnt.ExtraParams,
                Filename = anonyEvnt.Filename,
                DeviceSerialNumber = anonyEvnt.DeviceSerialNumber,
                //CODAmount = (decimal?)anonyEvnt.CODAmount,
            };
        }
    }
}
