﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DynamoDbViewer.Helpers;
using MahApps.Metro.Controls;

namespace DynamoDbViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        #region View related methods
        private void SettingsButton_OnClick(object sender, RoutedEventArgs e)
        {
            SettingsFlyout.IsOpen = (!SettingsFlyout.IsOpen);
        }
        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // this is a workaround, the proper way is to add a dependancy property to JsonViewer control, and bound it to JsonText vm property
            if (TreeViewTabItem.IsSelected && JsonTextBox.Text.ValidateJSON())
                JV.Load(JsonTextBox.Text);
        }
        private void TextBoxBase_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            //JsonTextBox.Clear();
        }
        #endregion
    }
}
