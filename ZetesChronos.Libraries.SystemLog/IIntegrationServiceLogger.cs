﻿namespace ZetesChronos.Libraries.SystemLog
{
    /// <summary>
    /// Interface to use as integration logger from the rest services.
    /// </summary>
    public interface IIntegrationServiceLogger : ILogger
    {
    }
}