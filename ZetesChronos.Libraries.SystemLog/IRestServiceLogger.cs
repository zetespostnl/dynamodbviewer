﻿namespace ZetesChronos.Libraries.SystemLog
{
    /// <summary>
    ///     Interface to use as generic logger from the rest services.
    /// </summary>
    public interface IRestServiceLogger : ILogger
    {
    }
}