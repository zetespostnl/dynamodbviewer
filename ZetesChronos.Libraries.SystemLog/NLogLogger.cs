﻿using System;
using NLog;

namespace ZetesChronos.Libraries.SystemLog
{
    public class NLogLogger : IRestServiceLogger, IIntegrationServiceLogger
    {
        private readonly Logger _logger;

        public NLogLogger(string loggerName)
        {
            _logger = LogManager.GetLogger(loggerName);
        }

        public void Debug(string message)
        {
            _logger.Debug(message);
        }

        public void Error(Exception exception, string message)
        {
            _logger.Error(exception, message);
        }

        public void Fatal(Exception exception, string message)
        {
            _logger.Fatal(exception, message);
        }

        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void Trace(string message)
        {
            _logger.Trace(message);
        }

        public void Warn(string message)
        {
            _logger.Warn(message);
        }
    }
}