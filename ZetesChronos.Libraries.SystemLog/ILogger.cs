﻿using System;

namespace ZetesChronos.Libraries.SystemLog
{
    /// <summary>
    /// Defines the operations in a logging interface.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Writes a diagnostic message into the Trace level.
        /// </summary>
        /// <param name="message">The message to write.</param>
        void Trace(string message);

        /// <summary>
        /// Writes a diagnostic message into the Debug level.
        /// </summary>
        /// <param name="message">The message to write.</param>
        void Debug(string message);

        /// <summary>
        /// Writes a diagnostic message into the Info level.
        /// </summary>
        /// <param name="message">The message to write.</param>
        void Info(string message);

        /// <summary>
        /// Writes a diagnostic message into the Warn level.
        /// </summary>
        /// <param name="message">The message to write.</param>
        void Warn(string message);

        /// <summary>
        /// Writes a diagnostic message into the Error level.
        /// </summary>
        /// <param name="exception">The <see cref="Exception"/> object.</param>
        /// <param name="message">The message to write.</param>
        void Error(Exception exception, string message);

        /// <summary>
        /// Writes a diagnostic message into the Fatal level.
        /// </summary>
        /// <param name="exception">The <see cref="Exception"/> object.</param>
        /// <param name="message">The message to write.</param>
        void Fatal(Exception exception, string message);
    }
}
