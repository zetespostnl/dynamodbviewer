﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Zetes.DomainModel.Entities.Execution.Events;
using Zetes.DomainModel.HostIntegration.Contracts;

namespace ZetesChronos.Integration.Host.LocalDrive
{
    public class LocalHostEventDispatcher : IHostExecutionEventsDispatcher
    {
        private string _localFile;

        public LocalHostEventDispatcher(string localFile)
        {
            _localFile = localFile;
        }

        #region Implementation of IHostExecutionEventsDispatcher

        public void SendAsync(ExecutionEventsContainer value)
        {
            Task.Run(() =>
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                try
                {
                    string msg = JsonConvert.SerializeObject(value);
                    System.IO.File.AppendAllText(_localFile, $"{msg}\n");
                    stopwatch.Stop();
                }
                catch (Exception exception)
                {
                    stopwatch.Stop();                    
                }
            });
        }

        #endregion
    }
}