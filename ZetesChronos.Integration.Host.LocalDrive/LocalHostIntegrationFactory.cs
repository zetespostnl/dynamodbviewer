﻿using System;
using Zetes.DomainModel.HostIntegration.Contracts;

namespace ZetesChronos.Integration.Host.LocalDrive
{
    public class LocalHostIntegrationFactory : IHostIntegrationFactory
    {
        private string _localFile;

        public LocalHostIntegrationFactory(string localFile)
        {
            _localFile = localFile;
        }

        public IHostExecutionEventsDispatcher CreateHostEventsDispatcher()
        {
            return new LocalHostEventDispatcher(_localFile);
        }
    }
}